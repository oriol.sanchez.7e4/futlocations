package com.example.futlocations.data

import android.graphics.Bitmap

data class MarkerMine (
    val id: Long,
    val longitud: Float,
    val latitud: Float,
    val markerName: String,
    val description: String,
    val photo: Bitmap?,
    val urlDownload: String
    )


