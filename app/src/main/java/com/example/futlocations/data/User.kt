package com.example.futlocations.data

data class User(var email: String?=null,
                var name: String?=null,
                var surname: String?=null)
