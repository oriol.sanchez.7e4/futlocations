package com.example.futlocations

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.futlocations.main.MainViewModel
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth


class LoginFragment : Fragment(R.layout.fragment_login) {
    lateinit var topAppBar: MaterialToolbar
    lateinit var email: TextInputLayout
    lateinit var password: TextInputLayout
    lateinit var registerLink: TextView
    lateinit var loginButton: Button
    private val viewModel: MainViewModel by activityViewModels()



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        topAppBar = requireActivity().findViewById(R.id.topAppBar)
        topAppBar.title = "Log In"

        topAppBar.visibility = View.GONE

        email = view.findViewById(R.id.email)
        password = view.findViewById(R.id.password)
        registerLink = view.findViewById(R.id.registerLink)
        loginButton = view.findViewById(R.id.loginButton)

        val user = FirebaseAuth.getInstance().currentUser
        if (user != null){
            viewModel.getMarkersFirebase()
            topAppBar.visibility = View.VISIBLE
            findNavController().navigate(R.id.mainFragment)
        }


        loginButton.setOnClickListener{
            FirebaseAuth.getInstance().
            signInWithEmailAndPassword(email.editText?.text.toString(), password.editText?.text.toString())
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        viewModel.getMarkersFirebase()
                        topAppBar.visibility = View.VISIBLE
                        viewModel.emailCurrent = email.editText?.text.toString()
                        findNavController().navigate(R.id.mainFragment)
                    }
                    else{
                        Toast.makeText(context, "Error al fer login", Toast.LENGTH_SHORT).show()
                    }
                }
        }

        registerLink.setOnClickListener {
            findNavController().navigate(R.id.registerFragment)
        }


    }
}