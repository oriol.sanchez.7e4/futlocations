package com.example.futlocations

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.ActivityNotFoundException
import android.content.ContentProviderClient
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.drawToBitmap
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.futlocations.data.MarkerMine
import com.example.futlocations.main.MainFragment
import com.example.futlocations.main.MainViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.google.firebase.storage.ktx.storage
import com.google.firebase.storage.ktx.storageMetadata
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.jar.Manifest

class AddMarkerFragment : Fragment(R.layout.fragment_add_marker) {
    lateinit var topAppBar: MaterialToolbar
    lateinit var longitud: TextInputLayout
    lateinit var latitud: TextInputLayout
    lateinit var markerName: TextInputLayout
    lateinit var description: TextInputLayout
    lateinit var camera: ImageButton
    lateinit var addButton: Button
    lateinit var coordButton: Button
    private var urlDownload: String = "https://firebasestorage.googleapis.com/v0/b/futlocation.appspot.com/o/images%2Fcamera.jpg?alt=media&token=ccad9185-21fe-4163-b18a-b5a6a8a3347f"

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient


    private val viewModel: MainViewModel by activityViewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        topAppBar = activity!!.findViewById(R.id.topAppBar)
        topAppBar.title = "Add Marker"

        longitud = view.findViewById(R.id.longitude)
        latitud = view.findViewById(R.id.latitude)
        markerName = view.findViewById(R.id.markerName)
        description = view.findViewById(R.id.description)
        camera = view.findViewById(R.id.camera)
        addButton = view.findViewById(R.id.addButton)
        coordButton = view.findViewById(R.id.coordenades)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context!!)

        longitud.editText?.setText(arguments?.getFloat("lng").toString())

        latitud.editText?.setText(arguments?.getFloat("lat").toString())

        coordButton.setOnClickListener{
            getLocations()
        }

        addButton.setOnClickListener {
            val marker = MarkerMine(
                viewModel.markers.size.toLong(),
                longitud.editText?.text.toString().toFloat(),
                latitud.editText?.text.toString().toFloat(),
                markerName.editText?.text.toString(),
                description.editText?.text.toString(),
                camera.drawToBitmap(),
                urlDownload
            )

            viewModel.addMarker(marker)


            val email = FirebaseAuth.getInstance().currentUser?.email
            Firebase.firestore.collection("users").document(email!!)
                .collection("markers").document(marker.id.toString())
                .set(
                    hashMapOf(
                        "name" to marker.markerName,
                        "description" to marker.description,
                        "coords" to GeoPoint(marker.latitud.toDouble(), marker.longitud.toDouble()),
                        "urlDownload" to urlDownload
                    )
                )

            findNavController().navigate(R.id.markersListFragment)

        }

        camera.setOnClickListener {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            try {
                startActivityForResult(takePictureIntent, 1)
            } catch (e: ActivityNotFoundException) {
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLocations() {
        fusedLocationProviderClient.lastLocation?.addOnSuccessListener {
            if (it == null){
                Toast.makeText(context, "Sorry can't get Location", Toast.LENGTH_SHORT).show()
            } else it.apply{
                longitud.editText?.setText(it.longitude.toString())
                latitud.editText?.setText(it.latitude.toString())
            }
        }

    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path =
            MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
       super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null){
            val imageBitmap = data!!.extras!!.get("data") as Bitmap
            camera.setImageBitmap(imageBitmap)
            uploadImageToFirebaseStorage(getImageUri(context!!, camera.drawToBitmap()))
            Log.d("Ress", "Photo was selected")
        }
    }

    private fun uploadImageToFirebaseStorage(uri: Uri){
        Log.d("selectedPhotoUri", "${uri}")

        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")

        ref.putFile(uri)
            .addOnSuccessListener {
                Log.d("addMarker", "Successfully uploaded image: ${it.metadata?.path}")

                ref.downloadUrl.addOnSuccessListener {
                    Log.d("urlDownload", "DOWNLOAD URL: ${urlDownload}")
                    urlDownload = it.toString()
                    Log.d("urlDownload", "DOWNLOAD URL: ${urlDownload}")
                }
            }
    }


}















