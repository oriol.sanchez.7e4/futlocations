package com.example.futlocations

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.futlocations.data.MarkerMine
import com.example.futlocations.main.MainViewModel

class MarkerAdapter(viewModel: MainViewModel, markers: List<MarkerMine>) : RecyclerView.Adapter<MarkerAdapter.WeaponListViewHolder>(), Filterable {
    private var viewModel = viewModel
    private var markerList = markers
    private var markerListFilter = markers

    class WeaponListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var markerName: TextView = itemView.findViewById(R.id.markerName)

        fun bindData(marker: MarkerMine) {
            markerName.text = marker.markerName
        }
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeaponListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_markeritem, parent, false)
        return WeaponListViewHolder(view)
    }

    override fun onBindViewHolder(holder: WeaponListViewHolder, position: Int) {
        holder.bindData(markerList[position])

        holder.itemView.setOnClickListener {
            viewModel.markerDetailed = markerList[position]
            viewModel.markerPosition = position

            Navigation.findNavController(holder.itemView).navigate(R.id.modMarkerFragment)
        }
    }

    override fun getItemCount(): Int {
        return markerList.size
    }

    override fun getFilter(): Filter {
        return object : Filter(){
            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val filterResults = FilterResults()
                if (charSequence == null || charSequence.length < 0){
                    filterResults.count = markerListFilter.size
                    filterResults.values = markerListFilter
                } else{
                    var searchChr = charSequence.toString().lowercase()
                    val markerModal = ArrayList<MarkerMine>()

                    for (marker in markerListFilter){
                        if (marker.markerName.lowercase().contains(searchChr)){
                            markerModal.add(marker)
                        }
                    }
                    filterResults.count = markerModal.size
                    filterResults.values = markerModal
                }

                return filterResults
            }

            override fun publishResults(p0: CharSequence?, filterResults: FilterResults?) {
                markerList = filterResults!!.values as ArrayList<MarkerMine>
                notifyDataSetChanged()
            }

        }
    }

    fun setData(list: List<MarkerMine>){
        this.markerList = list
        this.markerListFilter = list
        notifyDataSetChanged()
    }


//    fun setFavoriteList(weaponList: MutableList<WeaponEntity>){
//        viewModel.favorites.removeAll(viewModel.favorites)
//        for (i in weaponList){
//            for (j in weapons){
//                if (i.uuid==j.uuid){
//                    j.favorite = true
//                    viewModel.favorites.add(j)
//                    weaponFav.add(j)
//                }
//            }
//        }
//        Log.w("weaponList",""+weaponList)
//        notifyDataSetChanged()
//    }

}