package com.example.futlocations

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.futlocations.data.MarkerMine
import com.example.futlocations.main.MainViewModel
import com.google.android.material.appbar.MaterialToolbar

class MarkerListFragment : Fragment(R.layout.fragment_marker_list) {
    lateinit var topAppBar: MaterialToolbar
    private lateinit var searchBar: SearchView
    private lateinit var recyclerView: RecyclerView
    private val viewModel: MainViewModel by activityViewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.recycler_view)
        topAppBar = requireActivity().findViewById(R.id.topAppBar)
        topAppBar.title = "List Markers"


        searchBar = view.findViewById(R.id.svSearch)

        var markers = mutableListOf<MarkerMine>()
        markers.removeAll(markers)
        markers = viewModel.getMarkers()


        var adapter = MarkerAdapter(viewModel, markers)

        adapter.setData(markers)
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL ,false)
        recyclerView.adapter = adapter


        searchBar.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)

                topAppBar.title = "Search"
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                adapter.filter.filter(query)


                topAppBar.title = "List Markers"
                return false
            }


        })


    }



}