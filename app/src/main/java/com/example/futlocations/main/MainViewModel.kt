package com.example.futlocations.main

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.futlocations.data.MarkerMine
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class MainViewModel : ViewModel() {
    var markers = mutableListOf<MarkerMine>()
    var markersLiveData = MutableLiveData<List<MarkerMine>>()
    lateinit var markerDetailed: MarkerMine
    var markerPosition: Int = 0
    var emailCurrent: String? = null
    var latlngNeed = LatLng(0.0,0.0)
    var latlngRegiser = LatLng(41.4528056,2.185442)
    lateinit var latlng: LatLng

    @JvmName("getMarkers1")
    fun getMarkers() = markers

    fun getMarkersFirebase(){
        markers.clear()
        emailCurrent = FirebaseAuth.getInstance().currentUser?.email
        val email = FirebaseAuth.getInstance().currentUser?.email
        Firebase.firestore.collection("users").document(email!!)
            .collection("markers").get().addOnSuccessListener { list ->
                for (document in list) {

                    markers.add(
                        MarkerMine(
                            document.id.toLong(),
                            document.getGeoPoint("coords")!!.longitude.toFloat(),
                            document.getGeoPoint("coords")!!.latitude.toFloat(),
                            document.getString("name")!!,
                            document.getString("description")!!,
                            null,
                            document.getString("urlDownload")!!
                        )
                    )
                }
                getCoordsConfig()
                markersLiveData.postValue(markers)
            }

    }

    fun getCoordsConfig() {
        Firebase.firestore.collection("users").document(emailCurrent!!).get().addOnSuccessListener { document ->

            latlng = LatLng(
                document.getGeoPoint("coordsConfig")!!.latitude,
                document.getGeoPoint("coordsConfig")!!.longitude
            )

            Log.d("latlng", "$latlng")
        }
    }

    fun locateMarker(latLng: LatLng, map: GoogleMap){
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(latLng , 14.5f),
            3500, null)
    }


    fun addMarker(marker: MarkerMine){
        markers.add(marker)
    }

    fun modMarker(markerMine: MarkerMine, position: Int) {
        markers.removeAt(position)
        markers.add(markerMine)
    }

    fun delete(position: Int) {
        markers.removeAt(position)
    }


}