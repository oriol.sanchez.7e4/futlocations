package com.example.futlocations.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import com.example.futlocations.R
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth


class MainActivity : AppCompatActivity() {
    lateinit var topAppBar: MaterialToolbar
    lateinit var navigationView: NavigationView
    lateinit var drawerLayout: DrawerLayout
//    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        topAppBar = findViewById(R.id.topAppBar)
        navigationView = findViewById(R.id.navigation_view)
        drawerLayout = findViewById(R.id.drawerLayout)

        topAppBar.setNavigationOnClickListener {
            drawerLayout.open()
        }


        navigationView.setNavigationItemSelectedListener { menuItem ->
            when(menuItem.itemId){
                R.id.loginItem -> {
                    findNavController(R.id.nav_host_fragment).navigate(R.id.loginFragment)
                }
                R.id.mapaItem -> {
                    findNavController(R.id.nav_host_fragment).navigate(R.id.mainFragment)
                }
                R.id.addmarkerItem -> {
                    findNavController(R.id.nav_host_fragment).navigate(R.id.addMarkerFragment)
                }
                R.id.listMarkersItem -> {
                    findNavController(R.id.nav_host_fragment).navigate(R.id.markersListFragment)
                }
                R.id.configItem -> {
                    findNavController(R.id.nav_host_fragment).navigate(R.id.configFragment)
                }
                R.id.logoutItem -> {
                    FirebaseAuth.getInstance().signOut()
                    findNavController(R.id.nav_host_fragment).navigate(R.id.loginFragment)
                }
            }
            drawerLayout.close()
            true
        }

    }

}