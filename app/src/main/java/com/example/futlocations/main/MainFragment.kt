package com.example.futlocations.main

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.futlocations.R
import com.example.futlocations.data.MarkerMine
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.appbar.MaterialToolbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


class MainFragment : Fragment(), OnMapReadyCallback {
    lateinit var topAppBar: MaterialToolbar
    lateinit var map: GoogleMap
    lateinit var latlng: LatLng
    val REQUEST_CODE_LOCATION = 100



    companion object {
        fun newInstance() = MainFragment()
    }


    private val viewModel: MainViewModel by activityViewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        topAppBar = requireActivity().findViewById(R.id.topAppBar)
        topAppBar.title = "Mapa"


    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView =  inflater.inflate(R.layout.main_fragment, container, false)
        createMap()
        return rootView
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    fun createMap(){
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as     SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        enableLocation()

        map.setOnMapLongClickListener{
            Toast.makeText(context, "LAT: ${it.latitude} || LNG: ${it.longitude}", Toast.LENGTH_SHORT).show()
            val action =
                MainFragmentDirections.actionMainFragmentToAddMarkerFragment(
                    it.latitude.toFloat(),
                    it.longitude.toFloat()
                )
            findNavController().navigate(action)
        }

        viewModel.markersLiveData.observe(viewLifecycleOwner, {createMarker(it)})
    }

    fun createMarker(list: List<MarkerMine>){
        for(marker in list){
            val coordinates = LatLng(marker.latitud!!.toDouble(), marker.longitud.toDouble())
            val myMarker = MarkerOptions().position(coordinates).title(marker.markerName)
            Log.w("markerCreated", "" + myMarker)
            map.addMarker(myMarker)
        }

        if (viewModel.latlngNeed.latitude == 0.0){
            Firebase.firestore.collection("users").document(viewModel.emailCurrent!!).get().addOnSuccessListener { document ->
                viewModel.locateMarker(LatLng(document.getGeoPoint("coordsConfig")!!.latitude, document.getGeoPoint("coordsConfig")!!.longitude), map)
            }
        } else{
            viewModel.locateMarker(viewModel.latlngNeed, map)
        }
    }



    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    private fun enableLocation(){
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted()){
            map.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }

    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(requireContext(), "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when(requestCode){
            REQUEST_CODE_LOCATION -> if(grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED){
                map.isMyLocationEnabled = true
            }
            else{
                Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if(!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            map.isMyLocationEnabled = false
            Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                Toast.LENGTH_SHORT).show()
        }
    }






}