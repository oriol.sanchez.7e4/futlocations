package com.example.futlocations

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.Image
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.view.View.GONE
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.Toast
import androidx.core.view.drawToBitmap
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.futlocations.data.MarkerMine
import com.example.futlocations.main.MainViewModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import java.io.ByteArrayOutputStream
import java.util.*
import com.squareup.picasso.Picasso
import android.text.method.ScrollingMovementMethod
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices


class ModMarkerFragment : Fragment(R.layout.fragment_mod_marker) {
    lateinit var topAppBar: MaterialToolbar
    lateinit var marker: MarkerMine
    lateinit var longitud: TextInputLayout
    lateinit var latitud: TextInputLayout
    lateinit var markerName: TextInputLayout
    lateinit var description: TextInputLayout
    lateinit var camera: ImageButton
    lateinit var mapaButton: Button
    lateinit var addButton: Button
    lateinit var coordButton: Button
    private lateinit var deleteButton: Button
    private val viewModel: MainViewModel by activityViewModels()
    var urlDownload: String = "https://firebasestorage.googleapis.com/v0/b/futlocation.appspot.com/o/images%2Fcamera.jpg?alt=media&token=ccad9185-21fe-4163-b18a-b5a6a8a3347f"


    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        topAppBar = requireActivity().findViewById(R.id.topAppBar)
        topAppBar.title = "Modify Marker"

        longitud = view.findViewById(R.id.longitude)
        latitud = view.findViewById(R.id.latitude)
        markerName = view.findViewById(R.id.markerName)
        description = view.findViewById(R.id.description)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context!!)


        camera = view.findViewById(R.id.camera)
        mapaButton = view.findViewById(R.id.goMap)
        addButton = view.findViewById(R.id.addButton)
        coordButton = view.findViewById(R.id.coordenades)
        deleteButton = view.findViewById(R.id.delete)

        urlDownload = viewModel.markerDetailed.urlDownload

        marker = viewModel.markerDetailed

        longitud.editText?.setText(marker.longitud.toString())
        latitud.editText?.setText(marker.latitud.toString())
        markerName.editText?.setText(marker.markerName)
        description.editText?.setText(marker.description)

        Log.d("url: ", "" + marker.urlDownload)

        Picasso.get()
            .load(marker.urlDownload)
            .resize(350, 350)
            .centerCrop()
            .into(camera)

        mapaButton.setOnClickListener{
            viewModel.latlngNeed = LatLng(latitud.editText?.text.toString().toDouble(), longitud.editText?.text.toString().toDouble())
            findNavController().navigate(R.id.mainFragment)

        }

        coordButton.setOnClickListener{
            getLocations()
        }


        addButton.setOnClickListener {
            val marker = MarkerMine(
                viewModel.markerDetailed.id,
                longitud.editText?.text.toString().toFloat(),
                latitud.editText?.text.toString().toFloat(),
                markerName.editText?.text.toString(),
                description.editText?.text.toString(),
                camera.drawToBitmap(),
                urlDownload
            )
            viewModel.modMarker(marker, viewModel.markerPosition)

            val email = FirebaseAuth.getInstance().currentUser?.email
            Firebase.firestore.collection("users").document(email!!)
                .collection("markers").document(marker.id.toString())
                .update(
                    mapOf(
                        "name" to marker.markerName,
                        "description" to marker.description,
                        "coords" to GeoPoint(marker.latitud.toDouble(), marker.longitud.toDouble()),
                        "urlDownload" to urlDownload
                    )
                )

            findNavController().navigate(R.id.markersListFragment)
        }

        deleteButton.setOnClickListener{
            val email = FirebaseAuth.getInstance().currentUser?.email
            Firebase.firestore.collection("users").document(email!!)
                .collection("markers").document(marker.id.toString())
                .delete()

            viewModel.delete(viewModel.markerPosition)

            Toast.makeText(context, "Marker DELETED", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.markersListFragment)
        }

        camera.setOnClickListener{
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            try {
                startActivityForResult(takePictureIntent, 1)
            } catch (e: ActivityNotFoundException) {
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLocations() {
        fusedLocationProviderClient.lastLocation?.addOnSuccessListener {
            if (it == null){
                Toast.makeText(context, "Sorry can't get Location", Toast.LENGTH_SHORT).show()
            } else it.apply{
                longitud.editText?.setText(it.longitude.toString())
                latitud.editText?.setText(it.latitude.toString())
            }
        }

    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path =
            MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null){
            val imageBitmap = data!!.extras!!.get("data") as Bitmap
            camera.setImageBitmap(imageBitmap)
            uploadImageToFirebaseStorage(getImageUri(requireContext(), camera.drawToBitmap()))
            Log.d("Ress", "Photo was selected")
        }
    }


    private fun uploadImageToFirebaseStorage(uri: Uri){
        Log.d("selectedPhotoUri", "$uri")

        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")

        ref.putFile(uri)
            .addOnSuccessListener {
                Log.d("addMarker", "Successfully uploaded image: ${it.metadata?.path}")

                ref.downloadUrl.addOnSuccessListener {
                    Log.d("urlDownload", "DOWNLOAD URL: $urlDownload")
                    urlDownload = it.toString()
                    Log.d("urlDownload", "DOWNLOAD URL: $urlDownload")
                }
            }
    }
}