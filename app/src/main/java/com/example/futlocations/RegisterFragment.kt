package com.example.futlocations

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.futlocations.main.MainViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


class RegisterFragment : Fragment(R.layout.fragment_register) {
    private lateinit var topAppBar: MaterialToolbar
    lateinit var email: TextInputLayout
    private lateinit var password: TextInputLayout
    lateinit var name: TextInputLayout
    lateinit var surname: TextInputLayout
    lateinit var registerButton: Button

    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        topAppBar = requireActivity().findViewById(R.id.topAppBar)
        topAppBar.title = "Register"

        email = view.findViewById(R.id.email)
        password = view.findViewById(R.id.password)
        name = view.findViewById(R.id.name)
        surname = view.findViewById(R.id.surname)
        registerButton = view.findViewById(R.id.registerButton)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context!!)

        registerButton.setOnClickListener{
            createUser(email.editText?.text.toString(), password.editText?.text.toString(), name.editText?.text.toString(), surname.editText?.text.toString())
        }
    }

    fun createUser(email: String, password: String, name: String, surname: String){
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    Log.d("TRY", "$email, ${viewModel.latlngRegiser}, $name, $surname")
                    Firebase.firestore.collection("users").document(email).set(
                        hashMapOf(
                            "coordsConfig" to GeoPoint(viewModel.latlngRegiser.latitude, viewModel.latlngRegiser.longitude),
                            "name" to name,
                            "surname" to surname)
                    )
                    findNavController().navigate(R.id.loginFragment)
                } else {
                    Toast.makeText(context, "Error al registrar l'usuari", Toast.LENGTH_SHORT).show()
                }
            }
        viewModel.emailCurrent = email
        Toast.makeText(context, "User Created", Toast.LENGTH_SHORT).show()
    }
}