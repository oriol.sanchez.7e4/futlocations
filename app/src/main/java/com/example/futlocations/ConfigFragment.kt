package com.example.futlocations

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.futlocations.main.MainViewModel
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class ConfigFragment : Fragment(R.layout.fragment_config) {
    private lateinit var topAppBar: MaterialToolbar
    private lateinit var lat: TextInputLayout
    private lateinit var lng: TextInputLayout
    private lateinit var saveButton: Button

    private val viewModel: MainViewModel by activityViewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        topAppBar = requireActivity().findViewById(R.id.topAppBar)
        topAppBar.title = "Configuration"

        lat = view.findViewById(R.id.latitude)
        lng = view.findViewById(R.id.longitude)
        saveButton = view.findViewById(R.id.save)

        val latlngCurrent = viewModel.latlng

        lng.editText?.setText(latlngCurrent.longitude.toString())
        lat.editText?.setText(latlngCurrent.latitude.toString())

        saveButton.setOnClickListener{
            saveConfig(lat.editText?.text.toString(), lng.editText?.text.toString())
        }
    }

    fun saveConfig(lat: String, lng: String){
        val user = FirebaseAuth.getInstance().currentUser
        Firebase.firestore.collection("users").document(user!!.email!!)
            .update(
                mapOf(
                    "coordsConfig" to GeoPoint(lat.toDouble(), lng.toDouble())
                )
            ).addOnSuccessListener {
                Toast.makeText(context, "SAVED", Toast.LENGTH_SHORT).show()
            }
    }
}